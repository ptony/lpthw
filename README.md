# lpthw
*Learn Python the hard way !*

## Prérequis

* un terminal unix (Linux ou OSX)
* python 2 installé (pas python 3), `python --version`
* git installé, `git --version`
* un éditeur de texte (sublime-text par exemple)

## Exercices

1. Créer une copie de ce projet dans votre espace github en cliquant sur le bouton `Fork` en haut à droite de cette page.
2. Télécharger VOTRE version du projet sur votre machine depuis le terminal : `git clone https://github.com/VOTRE_IDENTIFIANT_GITHUB/lpthw`
3. Réaliser les exercices disponibles sur [le site Learn Python the Hard Way](http://learnpythonthehardway.org/book/)

Au fur et a mesure de votre progression vous allez créer les fichiers `ex1.py`, `ex2.py`, ... dans votre dossier `lpthw`

## Avant la fin de la journée

Il vous faut envoyer votre travail sur votre espace github pour que nous puissions évaluer votre progression.

### Configurer git

Configurer votre nom dans git sur votre machine

```
git config --global user.name "VOTRE NOM"
```

Configurer votre email
> Attention! Il faut utiliser le même email que celui de votre compte github.

```
git config --global user.email "VOTRE EMAIL"
```

### Sauvegarder et envoyer son travail sur github

* Enregistrez votre travail dans git en faisant un commit (en adaptant en fonctione des fichiers que vous avez dans votre dossier `lpthw`)

```
git add ex1.py ex2.py ex3.py
```
puis :

```
git commit -m "Fini les exercices 1 à 3"
```
* Envoyez votre travail dans votre espace personnel sur github :

```
git push origin master
```

## En cas de problème

* Recherchez via votre moteur de recherche préféré si il n'y a pas une solution évidente disponible sur internet
* Demandez à votre voisin.ne
* Si le problème est lié à git ou github, [voir l'aide de github](https://help.github.com/articles/set-up-git/)
* Si la commande `git push origin master` ne fonctionne pas demandez à un formateur pour qu'il vous aide à ajouter la clé ssh de votre PC à votre compte github ou [suivre l'aide](https://help.github.com/articles/generating-ssh-keys/).

